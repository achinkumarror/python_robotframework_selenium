*** Settings ***
#import libraries
Library  SeleniumLibrary

*** Variables ***
${browser}  firefox
${url}  https://demo.nopcommerce.com/


*** Test Cases ***
Fourth robottest case nopcommerce
    [documentation]  nopCommerce Store Demo
    [tags]  functionaltest

    Open Browser    ${url}   ${browser}
    loginToApplication          #reffering the user defined keyword created by user
    Close Browser

*** Keywords ***
loginToApplication
    Click link  xpath://a[@class='ico-login']
    Input text  id:Email    achinkumarror@gmail.com
    Input text  id:Password     Test123
    Click element   xpath://input[@class='button-1 login-button']