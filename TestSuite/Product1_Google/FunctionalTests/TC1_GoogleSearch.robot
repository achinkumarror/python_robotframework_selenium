*** Settings ***
#import libraries
Library  SeleniumLibrary


*** variables ***

*** Test Cases ***
First robottest case GoogleSearch
    [documentation]  GoogleTest
    [tags]  functionaltest

    Open Browser  http://google.com  chrome  #keyword Open Browser used to open browser
    Input Text  name:q  SeleniumHQ
    Input SendKeys  keys:ENTER
    Close Browser

Second robottest case nopcommerce
    [documentation]  nopCommerce Store Demo
    [tags]  functionaltest

    Open Browser  https://demo.nopcommerce.com/  firefox
    Click link  xpath://a[@class='ico-login']
    Input text  id:Email    achinkumarror@gmail.com
    Input text  id:Password     Test123
    Click element   xpath://input[@class='button-1 login-button']
    Close Browser


*** Keywords ***
