*** Settings ***
#import libraries
Library  SeleniumLibrary

*** variables ***
#user defined variables to be used in testcases.

*** Test Cases ***
Second robottest case nopcommerce
    [documentation]  nopCommerce Store Demo
    [tags]  functionaltest

    Open Browser  https://demo.nopcommerce.com/  firefox
    Click link  xpath://a[@class='ico-login']
    Input text  id:Email    achinkumarror@gmail.com
    Input text  id:Password     Test123
    Click element   xpath://input[@class='button-1 login-button']
    Close Browser

*** Keywords ***
#user deined keywords to be used in testcases.
