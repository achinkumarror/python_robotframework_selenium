*** Settings ***
#import libraries
Library  SeleniumLibrary

*** Variables ***
${browser}  firefox
${url}  https://demo.nopcommerce.com/


*** Test Cases ***
Third robottest case nopcommerce
    [documentation]  nopCommerce Store Demo
    [tags]  functionaltest

    Open Browser    ${url}   ${browser}
    Click link  xpath://a[@class='ico-login']
    Input text  id:Email    achinkumarror@gmail.com
    Input text  id:Password     Test123
    Click element   xpath://input[@class='button-1 login-button']
    Close Browser

*** Keywords ***
