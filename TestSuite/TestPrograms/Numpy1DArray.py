import numpy as np

a = np.array([0,1,2,3,4])
print(a)
print(a.size)
print(type(a))
print(a.dtype)
print(a.ndim) #dimensions of numpy array
print(a.shape) #shape/size of numpy array
print(a.mean())  #mean of numpy array
print(a.max())
print(a.min())
print(a[0])
print(a[0:3])  #Slicing

#Vector addition and subtraction
u = np.array([1,0])
v = np.array([0,1])
z=u+v
print(z)
x = u*v
print(x)


#using for loop
u=[0,1]
v=[1,0]
z=[]
for i,j in zip(u,v):
    z.append(i+j)

print(z)

#plot
a= np.array([1,0])
b=np.array([0,1])
Plotvec2(a,b)
