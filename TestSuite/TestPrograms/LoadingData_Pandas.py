#Read csv and Excel using pandas
#Loading data with pandas

import pandas as pd

df=pd.DataFrame({'a':[11,21,31],'b':[21,22,23]})
print(df.head())
print(df.head(1))
print(df.head(2))
print(df.head(3))


print("\n")
print(df['a'])

print("\n")
print(df.iloc[0,1])
print(df.iloc[1,0])

print("\n")
print(df['a']==1)


