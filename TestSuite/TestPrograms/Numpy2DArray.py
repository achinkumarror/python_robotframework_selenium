import numpy as np

a = np.array([[0,1,2],[3,4,5],[6,7,8]]) #2D Array
print(a)
print(a.size)
print(type(a))
print(a.dtype)

print(a.ndim) #dimensions of numpy array
print(a.shape) #shape/size of numpy array

print(a.mean())  #mean of numpy array
print(a.max())
print(a.min())

#Accessing different elements of a Numpy Array
print(a[0,0])
print(a[0,1])
print(a[1,1])
print(a[0][0:2])
print(a[1:3, 2])
print(a[0:3])  #Slicing

#Operation addition and subtraction
u = np.array([[1,0],[1,0]])
v = np.array([[0,1],[0,1]])
z=u+v
print(z)
x = u*v
print(x)


#using for loop
u=[[1,2],[3,4]]
v=[[0,5],[0,6]]
z=[]
for i,j in zip(u,v):
    z.append(i+j)

print(z)
#the numpy function dot to multiply the arrays together.
z = np.dot(u,v)
print(z)

#plot
a= np.array([1,0])
b=np.array([0,1])
Plotvec2(a,b)